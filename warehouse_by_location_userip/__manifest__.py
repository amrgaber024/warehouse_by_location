# -*- coding: utf-8 -*-
{
    'name': "Warehouse by location user IP ",

    "summary": "Change Company for user based on Current location ,"
               " warehouse automatic will be changed after changed company",
    'author': "Amr Gaber",
    'category': 'Sale',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    'external_dependencies':
        {
            'python': ['urllib', 'json'],
        },
}
