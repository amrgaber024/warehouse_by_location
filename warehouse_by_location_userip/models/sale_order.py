# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import json
from urllib.request import urlopen

class ResComapny(models.Model):
    _inherit = 'res.company'

    state_id = fields.Many2one('res.country.state', compute='_compute_address', inverse='_inverse_state',
                               string="Fed. State", store=True)
    country_id = fields.Many2one('res.country', compute='_compute_address', inverse='_inverse_country',
                                 string="Country", store=True)

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    company_id = fields.Many2one('res.company', 'Company', required=True, index=True,
                                 default=lambda self: self._get_company_by_location() or self.env.company)

    def _get_company_by_location(self):
        url = 'http://ipinfo.io/json'
        response = urlopen(url)
        data = json.load(response)
        region = data['region']
        country_code = data['country']
        if country_code and region:
            country = self.env['res.country'].search([('code', '=', country_code)], limit=1)
            if country:
                state = self.env['res.country.state'].search([('name', '=', region), ('country_id', '=', country.id)],
                                                             limit=1)
                print('country', country, state)
                if state:
                    domain = [('country_id', '=', country.id), ('state_id', '=', state.id)]
                    company = self.env['res.company'].search(domain, limit=1)
                    if company:
                        self.env.user.company_id = company.id
                        return company.id
        return False
